<?php

/**
 * @file
 * Both words with initial capital letters, last word in plural form.
 */

t('Ad-Blockers');
t('Administrator Accounts');
t('Administrator Emails');
t('Administrator Names');
t('Administrator Notes');
t('Administrator Roles');
t('Administrator Settings');                                                    // https://localize.drupal.org/translate/languages/hu/translate?sid=146512
t('Administrator Warnings');
t('Alert Boxes');
t('Alert Dangers');
t('Alert Descriptions');
t('Alert Information');
t('Alert Messages');
t('Alert Names');
t('Alert Segments');
t('Alert Styles');
t('Alert Successes');
t('Alert Texts');
t('Alert Times');
t('Alert Types');
t('Alert Warnings');
t("Alert's Heights");
t("Alert's Widths");
t('Author Blocks');
t('Author ID');                                                                 // https://localize.drupal.org/translate/languages/hu/translate?sid=279198
t('Author Links');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=1326593
t('Author Settings');                                                           // https://localize.drupal.org/translate/languages/hu/translate?sid=89260
t('Authoring Aliases');                                                         // https://localize.drupal.org/translate/languages/hu/translate?sid=124096
t('Authorized Amounts');
t('Authorized Codes');
t('Authorized Fields');
t('Authorized Redirects');
t('Authorized Users');
t('Authors / Editors');                                                         // https://localize.drupal.org/translate/languages/hu/translate?sid=264020
t('Code Snippets');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=461963
t('Database updates');                                                          // https://localize.drupal.org/translate/languages/hu/translate?sid=250532
t('Double Clicks');
t('Unauthorized Accesses');
t('Unauthorized Cards');
t('Unauthorized Items');
t('Unauthorized Origins');
t('Author Types');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=279316
t("Author's Posts");                                                            // https://localize.drupal.org/translate/languages/hu/translate?sid=343244
t('Author Bios');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=359086
t('Authored Nodes');                                                            // https://localize.drupal.org/translate/languages/hu/translate?sid=65734
t('Author Subscriptions');                                                      // https://localize.drupal.org/translate/languages/hu/translate?sid=66876
