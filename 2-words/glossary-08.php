<?php

/**
 * @file
 * No initial capital letter, last word in plural form.
 */

t('ad-blockers');
t('administrator accounts');
t('administrator emails');
t('administrator names');
t('administrator notes');
t('administrator roles');
t('administrator settings');
t('administrator warnings');
t('alert boxes');
t('alert dangers');
t('alert descriptions');
t('alert information');
t('alert keys');
t('alert messages');
t('alert names');
t('alert segments');
t('alert styles');
t('alert successes');
t('alert texts');
t('alert times');
t('alert types');
t('alert warnings');
t('alert\'s heights');
t('alert\'s widths');
t('author blocks');
t('author settings');
t('authoring aliases');
t('authorized amounts');
t('authorized codes');
t('authorized fields');
t('authorized redirects');
t('authorized users');
t('authors / editors');
t('code snippets');
t('double clicks');
t('taxonomy terms')                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=2524
t('unauthorized accesses');
t('unauthorized cards');
t('unauthorized items');
t('unauthorized origins');
