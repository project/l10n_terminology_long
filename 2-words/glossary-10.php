<?php

/**
 * @file
 * Only one initial capital letter, last word in singular form.
 */

t('Ad-blocker');
t('Administrator account');                                                     // https://localize.drupal.org/translate/languages/hu/translate?sid=248288
t('Administrator email');
t('Administrator name');
t('Administrator note');                                                        // https://localize.drupal.org/translate/languages/hu/translate?sid=146644
t('Administrator role');
t('Administrator setting');
t('Administrator warning');
t('Alert box');
t('Alert danger');
t('Alert description');
t('Alert info');
t('Alert key');                                                                 // https://localize.drupal.org/translate/languages/hu/translate?sid=2468358
t('Alert message');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=519794
t('Alert name');
t('Alert segment');
t('Alert style');
t('Alert success');
t('Alert text');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=2617781
t('Alert time');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=519784
t('Alert type');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=2688799
t('Alert warning');
t('Alert\'s height');                                                           // https://localize.drupal.org/translate/languages/hu/translate?sid=964049
t('Alert\'s width');                                                            // https://localize.drupal.org/translate/languages/hu/translate?sid=964044
t('Author affiliation');                                                        // https://localize.drupal.org/translate/languages/hu/translate?sid=926269
t('Author block');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=178232
t('Author e-mail');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=395503
t('Author email');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=262712
t('Author email');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=262712
t('Author filter.');                                                            // https://localize.drupal.org/translate/languages/hu/translate?sid=175692
t('Author filter');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=175530
t('Author id');                                                                 // https://localize.drupal.org/translate/languages/hu/translate?sid=926719
t('Author identifier.');                                                        // https://localize.drupal.org/translate/languages/hu/translate?sid=175634
t('Author information');                                                        // https://localize.drupal.org/translate/languages/hu/translate?sid=249946
t('Author leader');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=175372
t('Author link');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=321542
t('Author merge');
t('Author name');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=280530
t('Author path');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=301593
t('Author picture');                                                            // https://localize.drupal.org/translate/languages/hu/translate?sid=245398
t('Author queue');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=256910
t('Author taxonomy');                                                           // https://localize.drupal.org/translate/languages/hu/translate?sid=185204
t('Author textfield');                                                          // https://localize.drupal.org/translate/languages/hu/translate?sid=381963
t('Author type');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=279682
t('Author uid');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=1052628
t('Author: !name');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=164630
t('Author: !username');                                                         // https://localize.drupal.org/translate/languages/hu/translate?sid=425248
t('Author: @name');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=199738
t('Author: @name');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=199738
t('Author: %name');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=210454
t('Author: %name');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=210454
t('Author\'s email');                                                           // https://localize.drupal.org/translate/languages/hu/translate?sid=287320
t('Author\'s username');                                                        // https://localize.drupal.org/translate/languages/hu/translate?sid=391133
t('Author\'s website');                                                         // https://localize.drupal.org/translate/languages/hu/translate?sid=113264
t('Authoring alias');
t('Authoring date');                                                            // https://localize.drupal.org/translate/languages/hu/translate?sid=1333323
t('Authoring information.');                                                    // https://localize.drupal.org/translate/languages/hu/translate?sid=93406
t('Authoring information', [], ['context' => 'simplifying']);                   // https://localize.drupal.org/translate/languages/hu/translate?sid=2739923
t('Authoring information');                                                     // https://localize.drupal.org/translate/languages/hu/translate?sid=28072
t('Authoring infromation');                                                     // https://localize.drupal.org/translate/languages/hu/translate?sid=2712559
t('Authorized amount');                                                         // https://localize.drupal.org/translate/languages/hu/translate?sid=2581055
t('Authorized by');
t('Authorized code');                                                           // https://localize.drupal.org/translate/languages/hu/translate?sid=2684964
t('Authorized field');
t('Authorized redirect');                                                       // https://localize.drupal.org/translate/languages/hu/translate?sid=1459373
t('Authorized user');
t('Code snippet');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=170992
t('Display link');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=189798
t('Taxonomy term') "https://localize.drupal.org/translate/languages/hu/translate?sid=3302" t('Taxonomy Term') // https://localize.drupal.org/translate/languages/hu/translate?sid=278858
t('Unauthorized access');                                                       // https://localize.drupal.org/translate/languages/hu/translate?sid=2495481
t('Unauthorized card.');                                                        // https://localize.drupal.org/translate/languages/hu/translate?sid=2586316
t('Unauthorized card');
t('Unauthorized item (:code).', [':code' => '']);                               // https://localize.drupal.org/translate/languages/hu/translate?sid=2680429
t('Unauthorized item');                                                         // https://localize.drupal.org/translate/languages/hu/translate?sid=1616883
t('Unauthorized origin.');                                                      // https://localize.drupal.org/translate/languages/hu/translate?sid=841929
t('Unauthorized origin');
t('Verify code');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=2563221
t('Verifying entry...');                                                        // https://localize.drupal.org/translate/languages/hu/translate?sid=2656283
