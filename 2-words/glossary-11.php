<?php

/**
 * @file
 * Only one initial capital letter, last word in plural form.
 */

t('Ad-blockers');
t('Administrator accounts');
t('Administrator emails');
t('Administrator names');
t('Administrator notes');
t('Administrator roles');                                                       // https://localize.drupal.org/translate/languages/hu/translate?sid=121214
t('Administrator settings');                                                    // https://localize.drupal.org/translate/languages/hu/translate?sid=185678
t('Administrator warnings');
t('Alert boxes');
t('Alert dangers');
t('Alert descriptions');
t('Alert information');
t('Alert messages');
t('Alert names');
t('Alert segments');
t('Alert styles');
t('Alert successes');
t('Alert texts');
t('Alert times');
t('Alert type');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=2741414
t('Alert warnings');
t('Alert\'s heights');
t('Alert\'s widths');                                                           // https://localize.drupal.org/translate/languages/hu/translate?sid=964044
t('Author blocks');
t('Author group');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=72628
t('Author settings');                                                           // https://localize.drupal.org/translate/languages/hu/translate?sid=205320
t('Author tags');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=79572
t('Authoring aliases');
t('Authorized amounts');
t('Authorized codes');
t('Authorized fields');                                                         // https://localize.drupal.org/translate/languages/hu/translate?sid=604604
t('Authorized redirects');
t('Authorized users');                                                          // https://localize.drupal.org/translate/languages/hu/translate?sid=1103528
t('Code snippets');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=461928
t('Double clicks');
t('Taxonomy terms');                                                            // https://localize.drupal.org/translate/languages/hu/translate?sid=9442
t('Unauthorized accesses');
t('Unauthorized cards');
t('Unauthorized items');
t('Unauthorized origins');
t('Verify requirements');                                                       // https://localize.drupal.org/translate/languages/hu/translate?sid=248272
