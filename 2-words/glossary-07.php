<?php

/**
 * @file
 * No initial capital letter, last word in singular form.
 */

t('ad-blocker');
t('administrator account');
t('administrator email');
t('administrator name');
t('administrator note');
t('administrator role');
t('administrator setting');
t('administrator warning');
t('alert box');
t('alert danger');
t('alert description');
t('alert info');
t('alert key');
t('alert message');
t('alert name');
t('alert segment');
t('alert style');
t('alert success');
t('alert text');
t('alert time');
t('alert type');
t('alert warning');
t("alert's height");
t("alert's width");
t('author block');
t('author conference');                                                         // https://localize.drupal.org/translate/languages/hu/translate?sid=238098
t('author email');
t('author email');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=2755313
t('author information');
t('author interface');                                                          // https://localize.drupal.org/translate/languages/hu/translate?sid=303162
t('author link');
t('author merge');
t('author name');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=39420
t('author queue');
t('author rank');
t('author taxonomy');
t('author type');
t('author type');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=2755320
t('author uid');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=2097948
t('author uri');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=39424
t('authoring alias');
t('authoring information');
t('authorized amount');
t('authorized by');
t('authorized code');
t('authorized field');
t('authorized redirect');
t('authorized user');
t('code snippet');
t('double click');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=70
t('taxonomy term');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=69202
t('unauthorized access');
t('unauthorized cards');
t('unauthorized item');
t('unauthorized origin');
