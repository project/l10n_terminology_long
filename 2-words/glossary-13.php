<?php

/**
 * @file
 * Both words with initial capital letters, last word in singular form.
 */

t('Ad-Blocker');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=2673753
t('Administrator Account');
t('Administrator Email');
t('Administrator Name');                                                        // https://localize.drupal.org/translate/languages/hu/translate?sid=145846
t('Administrator Note');
t('Administrator Role');
t('Administrator Setting');
t('Administrator Warning');                                                     // https://localize.drupal.org/translate/languages/hu/translate?sid=164586
t('Alert Box');                                                                 // https://localize.drupal.org/translate/languages/hu/translate?sid=1791423
t('Alert Danger');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=2305093
t('Alert Description');                                                         // https://localize.drupal.org/translate/languages/hu/translate?sid=1705433
t('Alert Info');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=873774
t('Alert Message');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=544414
t('Alert Name');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=1705428
t('Alert Segment');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=1705423
t('Alert Style');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=2422926
t('Alert Success');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=2305083
t('Alert Text');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=2422928
t('Alert Time');
t('Alert Type');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=1705438
t('Alert Warning');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=2305088
t('Alert\'s Height');
t('Alert\'s Width');
t('Alerts Email');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=2406491
t('Author Block');
t('Author Email');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=577614
t('Author Information');                                                        // https://localize.drupal.org/translate/languages/hu/translate?sid=192248
t('Author Link');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=279102
t('Author Merge');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=279096
t('Author Pane');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=192234
t('Author Queue');
t('Author Rank');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=279204
t('Author Taxonomy');                                                           // https://localize.drupal.org/translate/languages/hu/translate?sid=185248
t('Author Type');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=279208
t('Author Uid');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=1471703
t('Author UID');                                                                // https://localize.drupal.org/translate/languages/hu/translate?sid=176530
t('Authoring Alias');
t('Authoring HTML');                                                            // https://localize.drupal.org/translate/languages/hu/translate?sid=1014079
t('Authoring Information');                                                     // https://localize.drupal.org/translate/languages/hu/translate?sid=2004328
t('Authoring<br />Infomation');                                                 // https://localize.drupal.org/translate/languages/hu/translate?sid=176044
t('Authorized Amount');
t('Authorized By');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=1608813
t('Authorized Code');
t('Authorized Field');
t('Authorized Redirect');
t('Authorized User');                                                           // https://localize.drupal.org/translate/languages/hu/translate?sid=2685484
t('Code Snippet');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=2037053
t('Double Click');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=72
t('Unauthorized Access');
t('Unauthorized Card');
t('Unauthorized Item');
t('Unauthorized Origin');
t('Author Lastname');                                                           // https://localize.drupal.org/translate/languages/hu/translate?sid=279216
t('Author Address');                                                            // https://localize.drupal.org/translate/languages/hu/translate?sid=279484
t('Author Type');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=279208
t('Author Name');                                                               // https://localize.drupal.org/translate/languages/hu/translate?sid=42970
t('Author Email');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=577614
t('Authors/Contributors');                                                      // https://localize.drupal.org/translate/languages/hu/translate?sid=342148
t('Author Deleted?');                                                           // https://localize.drupal.org/translate/languages/hu/translate?sid=356454
t('Author Brief');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=359090
t('Author E-mail');                                                             // https://localize.drupal.org/translate/languages/hu/translate?sid=146384
t('Author Affiliation');                                                        // https://localize.drupal.org/translate/languages/hu/translate?sid=146388
t('Author Contact');                                                            // https://localize.drupal.org/translate/languages/hu/translate?sid=140244
t('Author Facet');                                                              // https://localize.drupal.org/translate/languages/hu/translate?sid=57180
